"""Tests for triggers.stable_queue_trigger."""
import hashlib
import io
import subprocess
import unittest
import yaml

import mock

import triggers.stable_queue_trigger as stable_queue

import fakes


class TestHash(unittest.TestCase):
    """Tests for stable_queue.generate_hash()."""
    def test_hash_all(self):
        """
        Verify generate_hash() returns expected hash if all arguments are
        nonempty. Only make sure the function is not changed without people
        noticing since it can break checks of already tested queues!
        """
        branch, baseline, patches = 'branch', 'baseline', ['patch1', 'patch2']
        data = '{}@{}:{}'.format(branch, baseline, patches)
        self.assertEqual(hashlib.sha256(data.encode('utf-8')).hexdigest(),
                         stable_queue.generate_hash(branch, baseline, patches))

    def test_nones(self):
        """Verify generate_hash() works if any arguments are None."""
        data = '{}@{}:{}'.format(None, None, None)
        self.assertEqual(hashlib.sha256(data.encode('utf-8')).hexdigest(),
                         stable_queue.generate_hash(None, None, None))


class TestGetPatches(unittest.TestCase):
    """Tests for stable_queue.get_patches()."""
    @mock.patch('subprocess.check_call')
    def test_no_patches(self, mock_wget):
        """
        Check the series file retrieval fails if there are no new patches in
        the queue.
        """
        mock_wget.side_effect = subprocess.CalledProcessError(1, 'command')
        self.assertEqual([],
                         stable_queue.get_patches('repo', 'directory', 'hash'))

    @mock.patch('subprocess.Popen')
    @mock.patch('subprocess.check_call')
    def test_series_exist(self, mock_wget, mock_quilt):
        """Verify get_patches works with existing series file."""
        quilt_output = 'repo_url/plain/directory/PATCH\n'
        mock_quilt.return_value.stdout = io.BytesIO(quilt_output.encode())
        queue_hash = 'abcdef'

        self.assertEqual(
            [quilt_output.strip() + f'?id={queue_hash}'],
            stable_queue.get_patches('repo_url', 'directory', queue_hash)
        )
        mock_wget.assert_called_once()


class TestLoadTriggers(unittest.TestCase):
    """Tests for stable_queue.load_triggers()."""
    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.utils.was_tested')
    @mock.patch('triggers.stable_queue_trigger.get_patches')
    @mock.patch('triggers.stable_queue_trigger.generate_hash')
    @mock.patch('triggers.utils.get_tags')
    def test_set_values(self, mock_tags, mock_hash, mock_patches,
                        mock_tested, mock_commit):
        """
        Verify all required keys and values are set and renamed if the config
        is valid.
        """
        config_text = '{}'.format(
            'git_url: http://git.test/git/kernel.git\n'
            'cki_project: username/project\n'
            'cki_pipeline_branch: test_name\n'
            'queue_url: http://git-stable-queue.test/queue.git\n'
            'send_report_on_success: True\n'
            'test:\n'
            '  - xy\n'
        )
        config = yaml.safe_load(config_text)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_tags.return_value = []
        mock_hash.return_value = 'new_hash'
        mock_commit.side_effect = ['queue_hash', 'baseline_commit']
        mock_patches.return_value = ['patch-url1', 'patch-url2']
        mock_tested.return_value = False

        expected_trigger = {
            'git_url': 'http://git.test/git/kernel.git',
            'cki_pipeline_type': 'stable_queue',
            'commit_hash': 'baseline_commit',
            'queue_url': 'http://git-stable-queue.test/queue.git',
            'branch': 'linux-xy.y',
            'patch_urls': 'patch-url1 patch-url2',
            'cki_pipeline_id': 'new_hash',
            'cki_pipeline_branch': 'test_name',
            'send_report_on_success': 'True',
            'cki_project': 'username/project',
            'report_template': 'limited',
            'make_target': 'targz-pkg',
            'title': 'Stable queue: queue-xy',
            'subject': 'Stable queue: queue-xy',
            'name': 'stable_queue_xy',
            'queue_commit_hash': 'queue_hash'
        }

        triggers = stable_queue.load_triggers(gitlab, config, False)
        self.assertEqual(triggers, [expected_trigger])

    @mock.patch('triggers.utils.get_tags')
    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.stable_queue_trigger.get_patches')
    @mock.patch('triggers.stable_queue_trigger.generate_hash')
    def test_no_new_patches(self, mock_hash, mock_patches, mock_commit,
                            mock_tags):
        """Verify the trigger with no patches is not returned."""
        config_text = '{}'.format(
            'git_url: http://git.test/git/kernel.git\n'
            'cki_project: username/project\n'
            'cki_pipeline_branch: test_name\n'
            'queue_url: http://git-stable-queue.test/queue.git\n'
            'test:\n'
            '  - xy\n'
        )
        config = yaml.safe_load(config_text)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_tags.side_effect = [['vxyz', 'vyyy'], ['vxyz']]
        mock_hash.return_value = 'new_hash'
        mock_commit.side_effect = ['queue_hash', 'baseline_commit']
        mock_patches.return_value = []

        triggers = stable_queue.load_triggers(gitlab, config, False)
        self.assertEqual(triggers, [])

    @mock.patch('triggers.utils.get_tags')
    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.utils.was_tested')
    @mock.patch('triggers.stable_queue_trigger.get_patches')
    @mock.patch('triggers.stable_queue_trigger.generate_hash')
    def test_already_triggered(self, mock_hash, mock_patches, mock_tested,
                               mock_commit, mock_tags):
        """Verify already triggered pipeline is not triggered again."""
        config_text = '{}'.format(
            'git_url: http://git.test/git/kernel.git\n'
            'cki_project: username/project\n'
            'cki_pipeline_branch: test_name\n'
            'queue_url: http://git-stable-queue.test/queue.git\n'
            'test:\n'
            '  - xy\n'
        )
        config = yaml.safe_load(config_text)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_tags.side_effect = [['vxyz', 'vyyy'], ['vxyz']]
        mock_hash.return_value = 'new_hash'
        mock_commit.side_effect = ['queue_hash', 'baseline_commit']
        mock_patches.return_value = ['patch-url1', 'patch-url2']
        mock_tested.return_value = True

        triggers = stable_queue.load_triggers(gitlab, config, False)
        self.assertEqual(triggers, [])

    @mock.patch('triggers.utils.get_tags')
    @mock.patch('triggers.utils.get_commit_hash')
    def test_inconsistent(self, mock_commit, mock_tags):
        """Verify we don't trigger for inconsistent repo state."""
        config_text = '{}'.format(
            'git_url: http://git.test/git/kernel.git\n'
            'cki_project: username/project\n'
            'cki_pipeline_branch: test_name\n'
            'queue_url: http://git-stable-queue.test/queue.git\n'
            'test:\n'
            '  - xy\n'
        )
        config = yaml.safe_load(config_text)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_tags.side_effect = [['vxyz', 'vyyy'], ['vxyy']]
        mock_commit.return_value = 'queue_hash'

        triggers = stable_queue.load_triggers(gitlab, config, False)
        self.assertEqual(triggers, [])
