#!/bin/bash
set -euo pipefail

if [ -w '/etc/passwd' ]; then
    echo "cki:x:`id -u`:`id -g`:,,,:${HOME}:/bin/bash" >> /etc/passwd
fi

if [ -v SCHEDULE_BOT_DATA_DIR ]; then
    DATA_DIR="${SCHEDULE_BOT_DATA_DIR}"
elif [ -v CI_PROJECT_DIR ]; then
    DATA_DIR="${CI_PROJECT_DIR}"
else
    DATA_DIR=/data
fi

if [ -v TRIGGER_CONFIG_URL ]; then
    TRIGGER_CONFIG_FILENAME="${TRIGGER_CONFIG_URL##*/}"
    curl \
        --retry 5 \
        --connect-timeout 30 \
        --location \
        --silent \
        --show-error \
        --output "${DATA_DIR}/${TRIGGER_CONFIG_FILENAME}" \
        "${TRIGGER_CONFIG_URL}"
fi

python3 \
    -m triggers "${TRIGGER_TYPE}" \
    ${TRIGGER_OPTIONS:-} \
    -c "${DATA_DIR}/${TRIGGER_CONFIG_FILENAME}" | \
    tee -a "/logs/${TRIGGER_TYPE}.log"
