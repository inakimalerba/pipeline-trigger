"""
Pipeline trigger for stable queues.

1. Get the current stable queue for the requested version. The patches to be
   applied in the current version are in the queue-<version>/series file, which
   gets parsed.
2. Check if a pipeline with currently retrieved patches already ran.
3. Profit $$$.
"""
import hashlib
import io
import logging
import os
from pkg_resources import parse_version
import subprocess

from . import utils


def generate_hash(branch, commit_hash, patches):
    """
    Generate a hash used as cki_pipeline_id.

    Args:
        branch:      Git branch being tested.
        commit_hash: Commit which the patches are applied to.
        patches:     List of patch URLs applied to the baseline.

    Returns:
        sha256 digest of the formatted arguments.
    """
    string = f'{branch}@{commit_hash}:{patches}'
    return hashlib.sha256(string.encode('utf-8')).hexdigest()


def get_patches(repo_url, directory, queue_commit):
    """
    Get a list of patch URLs for given stable queue repo and directory
    associated with a release.

    Args:
        repo_url:     URL of the stable queue git repository.
        directory:    Directory in the stable queue repository associated with
                      tested kernel release.
        queue_commit: Current commit hash of the queue repository.

    Returns:
        A list of patch_urls
    """
    patch_urls = []

    # Since the 'series' file can contain comments (and eg. commented out
    # patches), get the actual list of patches to apply by parsing
    # 'quilt series' output. For this, we need to download the file first.
    series_url = f'{repo_url}/plain/{directory}/series?id={queue_commit}'
    try:
        subprocess.check_call(['wget', '-O', 'series', series_url])
    except subprocess.CalledProcessError:
        # The series file or the whole directory is not present, so there are
        # no patches in the queue for this release. Or the whole web with the
        # git tree is down. We don't care since a) we can't test anything
        # anyways, and b) we will be calling this trigger often enough that we
        # don't need a special code to handle this situation.
        logging.info('No series file for %s available', directory)
        return []

    quilt = subprocess.Popen(
        ['quilt', 'series'],
        stdout=subprocess.PIPE,
        env=dict(os.environ,
                 **{'QUILT_PATCHES': f'{repo_url}/plain/{directory}'})
    )
    for line in io.TextIOWrapper(quilt.stdout, encoding='utf-8'):
        line = line.strip()
        patch_urls.append(f'{line}?id={queue_commit}')

    return patch_urls


def load_triggers(gitlab_instance, stable_config, kickstart):
    triggers = []

    stable_config['cki_pipeline_type'] = 'stable_queue'
    # Don't send internal Beaker URLs to upstream lists
    stable_config['report_template'] = 'limited'

    for stable_version in stable_config['test']:
        trigger = stable_config.copy()
        project = gitlab_instance.projects.get(trigger['cki_project'])

        trigger['branch'] = f"linux-{stable_version}.y"
        queue_dir = f"queue-{stable_version}"
        trigger['name'] = f"stable_queue_{stable_version}"
        del trigger['test']

        # Get the stable queue revision that's used, to avoid dissapearing
        # patches when a new release is pushed during the testing. Also to
        # make it easy to associate a report with a queue version.
        trigger['queue_commit_hash'] = utils.get_commit_hash(
            trigger['queue_url'],
            'refs/heads/master'
        )
        if trigger['queue_commit_hash'] is None:
            continue

        logging.info('Current queue commit hash is %s',
                     trigger['queue_commit_hash'])

        # Get latest queue release to know where to apply patches in base
        # repo. Use the X.Z.y branch if no release is found.
        queue_tags = utils.get_tags(trigger['queue_url'])
        current_tags = list(filter(
            lambda l: l.startswith(f'v{stable_version}'),
            queue_tags
        ))
        if current_tags:
            newest_tag = sorted(current_tags, key=parse_version)[-1]
            # Is this tag actually present in the base repo? If not, don't
            # trigger the pipeline as it would just crash!
            base_tags = utils.get_tags(trigger['git_url'])
            if newest_tag not in base_tags:
                logging.info('Inconsistent repo for %s, not triggering!',
                             queue_dir)
                continue

            trigger['commit_hash'] = utils.get_commit_hash(
                trigger['git_url'], f'refs/tags/{newest_tag}'
            )
        else:
            trigger['commit_hash'] = utils.get_commit_hash(
                trigger['git_url'], f'refs/heads/{trigger["branch"]}'
            )
        if trigger['commit_hash'] is None:
            continue

        patch_urls = get_patches(trigger['queue_url'],
                                 queue_dir,
                                 trigger['queue_commit_hash'])
        trigger['patch_urls'] = ' '.join(patch_urls)

        if not trigger['patch_urls']:  # No patches to test
            continue

        # Remove the queue hash parts from patch URLs for pipeline ID
        # generation. If the queue commit hash is different it doesn't
        # necessarily mean that this queue was changed. It can be a
        # different one and we shouldn't retrigger that same pipeline
        # again.
        trigger['cki_pipeline_id'] = generate_hash(
            trigger['branch'],
            trigger['commit_hash'],
            trigger['patch_urls'].replace(
                f'?id={trigger["queue_commit_hash"]}',
                ''
            )
        )
        trigger['send_report_on_success'] = str(
            trigger.get('send_report_on_success', False)
        )

        if not trigger.get('make_target'):
            trigger['make_target'] = 'targz-pkg'

        do_not_trigger = utils.was_tested(project,
                                          trigger['cki_pipeline_branch'],
                                          trigger['cki_pipeline_id'])
        if do_not_trigger:
            logging.info('Pipeline for %s queue already triggered.',
                         trigger['branch'])
        else:
            trigger['title'] = 'Stable queue: {}'.format(queue_dir)
            trigger['subject'] = trigger['title']
            triggers.append(trigger)

    return triggers
