"""Tests for triggers.utils."""
import json
import os
import unittest

import mock
import requests
import responses

import triggers.utils as utils

import fakes


class TestSeriesData(unittest.TestCase):
    """Test cases for utils.SeriesData."""
    def test_repr_format(self):
        """Verify the string version of the objects contains everything."""
        data = utils.SeriesData(
            patches=['patch'],
            emails=set([]),
            subject='patch subject',
            series_id='123',
            message_id='<message@id>',
            last_tested='yesterday',
            submitter="joeuser@redhat.com"
        )
        attributes = ['Message ID', 'Subject', 'Cover letter', 'Patches',
                      'Last tested', 'Retrieved emails']

        data_text = repr(data)
        for attribute in attributes:
            self.assertIn(attribute, data_text)


class TestGetEnvVarOrRaise(unittest.TestCase):
    """Test cases for utils.get_env_var_or_raise()."""
    def test_existing_variable(self):
        """Test the function retrieves the value of existing variable."""
        os.environ.update({'TEST_EXISTING': 'test_value'})
        self.assertEqual(utils.get_env_var_or_raise('TEST_EXISTING'),
                         'test_value')
        del os.environ['TEST_EXISTING']

    def test_nonexistent_variable(self):
        """
        Test the function raises EnvVarNotSetError if the variable doesn't
        exist.
        """
        if os.environ.get('TEST_NONEXISTENT'):
            del os.environ['TEST_NONEXISTENT']

        with self.assertRaises(utils.EnvVarNotSetError):
            utils.get_env_var_or_raise('TEST_NONEXISTENT')


class TestCheckForTested(unittest.TestCase):
    """Tests for utils.was_tested()."""
    @mock.patch('triggers.utils.get_variables')
    def test_was_tested_true(self, mock_variables):
        """Check was_tested returns True if the pipeline already ran."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_2', 'token', {'ref': 'cki_2'})

        mock_variables.return_value = {'cki_pipeline_id': 'id',
                                       'ref': 'sha',
                                       'commit_hash': 'sha',
                                       'branch': 'git_branch'}

        self.assertTrue(utils.was_tested(project, 'cki_2', 'id'))
        mock_variables.assert_called_once()

    @mock.patch('triggers.utils.get_variables')
    def test_was_tested_false(self, mock_variables):
        """Check was_tested returns False if the pipeline didn't run."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'cki_pipeline_id': 'different_id',
                                       'ref': 'sha',
                                       'commit_hash': 'sha',
                                       'branch': 'git_branch'}

        self.assertFalse(utils.was_tested(project, 'cki_branch', 'id'))
        self.assertEqual(mock_variables.call_count, 2)


class TestGetCommitHash(unittest.TestCase):
    """Tests for utils.get_commit_hash()."""
    @mock.patch('subprocess.check_output')
    def test_commit_hash(self, mock_check_output):
        """Verify get_commit_hash returns the right value from git output."""
        mock_check_output.return_value = 'hash\t\trefs/heads/master'.encode()
        self.assertEqual(utils.get_commit_hash('repo', 'refs/heads/master'),
                         'hash')


class TestGetTags(unittest.TestCase):
    """Tests for utils.get_tags()."""
    @mock.patch('subprocess.check_output')
    def test_commit_hash(self, mock_check_output):
        """Verify get_tags returns the right values from git output."""
        git_data = 'hash\t\trefs/tags/v5.2.1\nhash2\trefs/tags/v5.2.1^{}'
        mock_check_output.return_value = git_data.encode()
        self.assertEqual(utils.get_tags('repo'), ['v5.2.1'])


class TestGenerateCIHash(unittest.TestCase):
    """Tests for utils.generate_ci_hash()."""
    def test_nonempty(self):
        """Verify expected hash is returned is all arguments are nonempty."""
        project = 'project/repo'
        pr_id = 23
        comment_id = 984

        self.assertEqual(
            '7058975511d2e26b4c92f5e1dbf0e27bc57d3b14e4a394386e23b4ccd077d750',
            utils.generate_ci_hash(project, pr_id, comment_id)
        )


class TestSanitizeEmails(unittest.TestCase):
    """Tests for patch_trigger.sanitize_emails()."""
    def test_missing(self):
        """Verify sanitize_emails works even if there are no such headers."""
        self.assertEqual(set(), utils.get_emails_from_headers({}))

    def test_with_data(self):
        headers = {'HeaderX': 'invalid', 'From': 'Name Surname <name@email>',
                   'To': 'Alias <name@email>, someoneelse@email'}
        self.assertEqual(set(['name@email', 'someoneelse@email']),
                         utils.get_emails_from_headers(headers))
