"""Common main() for all triggers."""
import os
import argparse
import logging
import sys
from yaml import load, Loader

import gitlab
import sentry_sdk

from . import TRIGGERS
from .utils import get_env_var_or_raise
from pipeline_tools.utils import trigger_pipelines


class StoreNameValuePair(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        variables = namespace.variables or {}
        for key_value_pair in values:
            key, value = key_value_pair.split('=')
            variables[key] = value
        namespace.variables = variables


def main():
    if 'SENTRY_DSN' in os.environ:
        # the DSN is read from SENTRY_DSN env variable
        sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))

    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.DEBUG,
                        stream=sys.stdout)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("github").setLevel(logging.WARNING)

    parser = argparse.ArgumentParser(description='Pipeline triggers')
    parser.add_argument(
        '-c', '--config',
        type=str,
        help=(
            'YAML configuration file to use. Will try to use `<name>.yaml` '
            'from the current working directory if not specified, where '
            '`<name>` is one of: {}'.format(list(TRIGGERS.keys()))
        )
    )
    parser.add_argument(
        '--token',
        type=str,
        default='GITLAB_TRIGGER_TOKEN',
        help=(
            'Name of the env var which contains GitLab trigger token. Defaults'
            ' to `GITLAB_TRIGGER_TOKEN`.'
        )
    )
    parser.add_argument('trigger',
                        choices=TRIGGERS.keys(),
                        help=('Name of the pipeline trigger'))
    parser.add_argument(
        '--kickstart',
        action='store_true',
        help=('Kickstart the pipelines. Instead of checking for the previously'
              ' existing pipelines, submit the newest available one, without '
              'sending the emails. Note that this created pipeline might be '
              'invalid. Not all triggers support this option.')
    )
    parser.add_argument(
        "--variables",
        action=StoreNameValuePair,
        nargs='+',
        help=(
            'Variables to be added/overriden in all pipelines triggered '
            'e.g. --variables skip_beaker=true --variables mail_to=nobody'
        ),
        default={},
    )
    args = parser.parse_args()

    gitlab_url = get_env_var_or_raise('GITLAB_URL')
    private_token = get_env_var_or_raise('GITLAB_PRIVATE_TOKEN')
    trigger_token = get_env_var_or_raise(args.token)
    configfile = args.config if args.config else '{}.yaml'.format(args.trigger)

    with open(configfile) as config_file:
        config = load(config_file, Loader=Loader)

    gitlab_instance = gitlab.Gitlab(gitlab_url,
                                    private_token=private_token,
                                    api_version=4)

    if args.trigger == 'brew':
        # This one should run all the time and trigger the pipeline itself
        TRIGGERS[args.trigger].poll_triggers(gitlab_instance,
                                             config,
                                             trigger_token)
    else:
        loader = TRIGGERS[args.trigger].load_triggers
        triggers = loader(gitlab_instance, config, args.kickstart)
        trigger_pipelines(
            gitlab_instance, trigger_token, triggers, args.variables
        )


if __name__ == '__main__':
    main()
