"""Pipeline triggers."""
from . import baseline_trigger
from . import brew_trigger
from . import patch_trigger
from . import patch_pw1_trigger
from . import stable_queue_trigger
from . import cki_github


TRIGGERS = {
    'baseline': baseline_trigger,
    'patch': patch_trigger,
    'patch-pw1': patch_pw1_trigger,
    'cki-github': cki_github,
    'brew': brew_trigger,
    'stable-queue': stable_queue_trigger
}
