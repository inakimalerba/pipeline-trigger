"""Pipeline trigger for GitHub repositories."""
import copy
import json
import logging
import os
import subprocess

import dateutil.parser
import github

from . import utils
from pipeline_tools.retrigger import run_directly_tests


def is_pr_already_reviewed(pull, comment):
    """
    Check if the pull request in question was already reviewed by the bot.

    Args:
        pull:    PullRequest instance being tested.
        comment: IssueComment instance that made the request on the PR.

    Returns:
        True if PR was already reviewed, otherwise False.
    """
    for review in pull.get_reviews():
        if comment.html_url in review.body:
            return True
    return False


def update_pull_request(pull, comment, cki_status, pipelines_pr):
    """
    Update the pull request, approving or requesting changes based on the
    cki_status passed as parameter.

    Args:
        pull:         PullRequest instance being tested
        comment:      IssueComment instance that made the request on the PR.
        cki_status:   String summarizing the status for all pipelines
                      triggered.
        pipelines_pr: A list of pipelines triggered by the PR.
    """
    if is_pr_already_reviewed(pull, comment):
        return
    if cki_status == 'passed':
        event = 'APPROVE'
        emoji = ':+1::beers:'
    else:
        event = 'REQUEST_CHANGES'
        emoji = ':-1::cry:'
    pipelines_url = '\n'.join([pipeline.web_url for pipeline in pipelines_pr])
    msg = f'{emoji} CKI pipeline {cki_status} {comment.html_url}'
    msg += f'\n\n{pipelines_url}'
    head_commit = pull.head.repo.get_commit(pull.head.sha)
    pull.create_review(head_commit, body=msg, event=event)


def get_variables(pipeline):
    """
    Cache the result of utils.get_variables.

    Args:
        pipeline: Object representing the pipeline to retrieve variables for.

    Returns:
        A dictionary representing job's variables.
    """
    os.makedirs('cache', exist_ok=True)
    path = os.path.join('cache', f'{pipeline.id}.json')
    vars = None
    try:
        with open(path) as file:
            vars = json.load(file)
    except (FileNotFoundError, json.JSONDecodeError):
        """Cache missing or corrupted"""
    if not vars:
        vars = utils.get_variables(pipeline)
        with open(path, 'w') as file:
            json.dump(vars, file)
    return vars


def get_last_succesful_baseline(project, cki_pipeline_branch):
    """
    Search for the last successful baseline filtering by cki_pipeline_branch.

    Args:
        project:             Gitlab API instance of the cki pipeline project.
        cki_pipeline_branch: CKI pipeline branch.

    Returns:
        A pipeline instance corresponding to the successful pipeline, otherwise
        None.
    """
    for pipeline in project.pipelines.list(as_list=False, status='success',
                                           scope='finished',
                                           ref=cki_pipeline_branch):
        variables = get_variables(pipeline)
        if variables.get('cki_pipeline_type') == 'baseline':
            return pipeline

    return None


def get_cki_pipeline_status(pull, comment, config, gitlab_instance):
    """
    Get the status for every cki pipeline triggered by the PR and summarize it
    in a string.

    Args:
        pull:            PullRequest instance being tested.
        comment:         IssueComment instance that made the request on the PR.
        gitlab_instance: gitlab.Gitlab API instance.

    Returns:
        A tuple with two values. The first member is a string representing the
        status for all pipelines and where the possible values are "running",
        "failed" and "passed". The second member is a list containing the cki
        pipelines.
    """
    project = gitlab_instance.projects.get(config['cki_project'])
    pipelines_pr = []
    for pipeline in project.pipelines.list(as_list=False):
        variables = get_variables(pipeline)
        if variables.get('pr_number') == str(pull.number):
            if variables.get('comment_id') == str(comment.id):
                pipelines_pr.append(pipeline)

        if len(pipelines_pr) >= len(config['cki_pipeline_branches']):
            break
    else:
        return (None, None)

    pipeline_running = False
    pipeline_failed = False
    for pipeline in pipelines_pr:
        if pipeline.status == 'running':
            pipeline_running = True
        elif pipeline.status != 'success':
            pipeline_failed = True

    if pipeline_running:
        return ('running', pipelines_pr)
    elif pipeline_failed:
        return ('failed', pipelines_pr)
    return ('passed', pipelines_pr)


def is_comment_already_processed(bot_login, comment):
    """
    Iterate over the comment's reactions to see if the comment was already
    processed.

    Args:
        bot_login: Bot Github's username.
        comment:   IssueComment instance.

    Returns:
        Reaction object if bot already put a reaction in the comment,
        otherwise False.
    """
    for reaction in comment.get_reactions():
        if reaction.user.login == bot_login:
            return reaction
    return False


def check_pipelines_for_update_pr(pull, comment, config, gitlab_instance):
    """
    Check the status for all pipelines and update the PR if they are finished.

    Args:
        pull:            PullRequest instance being tested.
        comment:         IssueComment instance that made the request on the PR.
        config:          Dictionary with the trigger configuration.
        gitlab_instance: gitlab.Gitlab API instance.
    """
    if is_pr_already_reviewed(pull, comment):
        return
    status, pipelines_pr = get_cki_pipeline_status(pull, comment, config,
                                                   gitlab_instance)
    logging.info('CKI pipeline status for PR %d %r', pull.number, status)
    if status and status != 'running':
        update_pull_request(pull, comment, status, pipelines_pr)


# TODO (TiN): Add support for setting variables (e.g. choosing a different
# kpet-db branch)
def process_comment(repo, pull, comment, config, gitlab_instance):
    """
    Build new triggers for the CKI pipeline.

    Args:
        repo:            Repository instance being tested.
        pull:            PullRequest instance being tested.
        comment:         IssueComment instance that made the request on the PR.
        config:          Dictionary with the trigger configuration.
        gitlab_instance: gitlab.Gitlab API instance.

    Returns:
        A list of triggers if the comment is processed, otherwise None.
    """
    triggers = []
    logging.info('processing %s', comment.body)
    project = gitlab_instance.projects.get(config['cki_project'])
    for branch in config['cki_pipeline_branches']:
        pipeline = get_last_succesful_baseline(project, branch)
        variables = get_variables(pipeline)
        variables['cki_project'] = config['cki_project']
        variables['pr_number'] = str(pull.number)
        variables['comment_id'] = str(comment.id)
        variables['github_project'] = config['github_project_path']
        var = config['variable_to_replace']
        fmt = config['format_to_replace']
        variables[var] = fmt.format(**locals())
        variables['require_manual_review'] = 'False'
        variables['cki_pipeline_type'] = f'test-{repo.name}@{pull.head.ref}'
        variables['title'] = 'Retrigger {}: {}'.format(
            variables['cki_pipeline_type'],
            variables['title'],
        )
        variables['retrigger'] = 'true'
        variables['cki_pipeline_id'] = utils.generate_ci_hash(
            config['github_project_path'], pull.id, comment.id
        )
        variables.update(config.get('variables', {}))

        run_directly_tests(project, pipeline, variables)

        logging.info('New trigger with variables %r', variables)
        triggers.append(variables)
    comment.create_reaction('+1')
    pull.create_issue_comment("CKI pipeline triggered")
    return triggers


def get_acceptable_askers(ghub, allowed):
    """
    Get a list of users that are allowed to trigger testing.

    Args:
        ghub:    GitHub object.
        allowed: Organization or username of people allowed to trigger tests.

    Returns:
        List of strings representing logins of allowed users.
    """
    try:
        organization = ghub.get_organization(allowed)
        return [user.login for user in organization.get_members()]
    except github.UnknownObjectException:
        # This is a user, not an organization
        return [allowed]


def was_force_pushed(repo, until, pr_id):
    """
    Check if the PR was updated by a force push after the specified time.

    Args:
        repo:  Object representing the GitHub repository.
        until: Datetime object representing the time of the last event to
               check.
        pr_id: ID of the PR to check events for.
    """
    for event in repo.get_issues_events():
        if event.created_at < until:
            return False

        if event.event == 'head_ref_force_pushed' and \
                event.issue.number == pr_id:
            return True


def was_committed(pull, since):
    """
    Check if the PR has any new commits since given datetime.

    Args:
        pull:  Object representing the pull request.
        since: Datetime since when to check new commits.

    Returns:
        True if there are any newer commits,
        False otherwise.
    """
    newest_commit_hash = pull.get_commits().reversed[0].sha
    source_repo = pull.head.repo
    commit = source_repo.get_commit(newest_commit_hash)

    # The timezone is UTC for all events but only some data contain it in the
    # returned datetime object. Because consistency is for noobs.
    if dateutil.parser.parse(commit.last_modified, ignoretz=True) > since:
        return True
    return False


def load_triggers(gitlab_instance, cki_github_config, kickstart):
    """
    Get ready all potential triggers.
    """
    triggers = []
    github_private_token = utils.get_env_var_or_raise('GITHUB_PRIVATE_TOKEN')
    ghub = github.Github(github_private_token)
    bot_login = ghub.get_user().login
    for key, value in cki_github_config.items():
        repo = ghub.get_repo(value['github_project_path'])
        acceptable_askers = value['requestors']
        for pull in repo.get_pulls():
            logging.info('%r', pull)
            involved = False
            post_bad_logins = set([])
            post_pull_updated = False
            for comment in pull.get_issue_comments():
                if comment.user.login == bot_login:
                    involved = True
                if f'@{bot_login}' in comment.body and 'test' in comment.body:
                    reaction = is_comment_already_processed(bot_login, comment)
                    if reaction:
                        if reaction.content == '+1':
                            # The comment triggered some pipelines
                            check_pipelines_for_update_pr(pull, comment, value,
                                                          gitlab_instance)
                        continue

                    ok_askers = get_acceptable_askers(ghub, acceptable_askers)
                    if comment.user.login not in ok_askers:
                        # This user is not allowed to trigger tests
                        comment.create_reaction('-1')
                        post_bad_logins.add(comment.user.login)
                        continue

                    was_forced = was_force_pushed(repo,
                                                  comment.created_at,
                                                  pull.number)
                    new_commits = was_committed(pull, comment.created_at)
                    if was_forced or new_commits:
                        comment.create_reaction('-1')
                        post_pull_updated = True
                        continue

                    # If we got here it means a pipeline should be triggered or
                    # updated. Ignore all previous "bad" attempts.
                    involved = True
                    post_bad_logins = []
                    post_pull_updated = False
                    pull_request_triggers = process_comment(
                        repo, pull, comment, value, gitlab_instance
                    )
                    if pull_request_triggers:
                        triggers.extend(pull_request_triggers)

            if post_bad_logins:
                bad_users = ' @'.join(post_bad_logins)
                pull.create_issue_comment(
                    f'Hi {bad_users}! You don\'t have permissions to trigger '
                    'testing. Please wait for a maintainer to review your PR.'
                )
            elif post_pull_updated:
                pull.create_issue_comment(
                    'Hi! The PR code has been modified since testing was '
                    'requested. Please review the new changes before asking '
                    'again to test.'
                )
            elif not involved and pull.state == "open":
                pull.create_issue_comment(
                    'Hi! This is the friendly CKI test bot. The maintainers '
                    'can mention me in a comment together with the word '
                    '\"test\" and I will test this PR and post the results. '
                    'Please note that the only tests that will be tested by '
                    'the bot are those that are enabled in kpet-db or are '
                    'already in use within CKI pipelines. If your test is '
                    'new and has not been enabled for use in pipelines, the '
                    'bot cannot test it here.'
                )
    return triggers
