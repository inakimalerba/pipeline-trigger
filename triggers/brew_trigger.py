"""
Trigger for Brew/Koji builds using UMB. WIP.
See https://pagure.io/fedora-ci/messages for details.
"""
import base64
from concurrent.futures import as_completed, ThreadPoolExecutor
import copy
import json
import logging
import lzma
import os
from queue import Queue
import re
import subprocess
from time import sleep

from cki_lib.misc import retry_safe_popen
from pipeline_tools.brew_trigger import get_brew_trigger_variables
import pipeline_tools.utils as pt_utils
import proton
import proton.handlers
from proton.reactor import Container
import zmq

from . import utils


def queue_watcher(queue):
    """Once a minute, log the queue length."""
    while True:
        logging.info('queue_size=%i', queue.qsize())
        sleep(60)


def gitlab_trigger(gitlab_queue, gitlab_instance):
    """Process GitLab trigger requests."""
    while True:
        trigger_token, trigger_to_use = gitlab_queue.get()
        try:
            if not os.environ.get('IS_PRODUCTION'):
                pt_utils.clean_variables(trigger_to_use)
                sleep(30)  # try to avoid collision with production deployment
            while os.path.exists('/run/pipeline-trigger/brew.suspend'):
                logging.info('Triggering suspended, sleeping')
                sleep(60)
            pt_utils.trigger_pipelines(gitlab_instance, trigger_token,
                                       [trigger_to_use])
            gitlab_queue.task_done()
        except Exception:  # pylint: disable=broad-except
            logging.exception('Unable to trigger pipeline for %s',
                              trigger_to_use)


def put_gitlab_queue(gitlab_queue, trigger_token, trigger_to_use):
    """Put a GitLab trigger request in a queue (for easy mocking)."""
    gitlab_queue.put((trigger_token, trigger_to_use))


def process_queue(queue, gitlab_queue, trigger_token, triggers,
                  brew_config):
    while True:
        message, server_section = queue.get()
        if 'copr' in message:
            process_copr(gitlab_queue,
                         trigger_token,
                         triggers,
                         message)
        elif 'cki_pipeline_id' in message:
            process_result_message(gitlab_queue,
                                   trigger_token,
                                   triggers,
                                   message)
        else:
            process_message(gitlab_queue,
                            trigger_token,
                            brew_config,
                            server_section,
                            message)
        queue.task_done()


def sanity_check(properties):
    """
    Verify the message is what we want.

    Args:
        properties: A dictionary of message properties.

    Returns:
        False if the message is "bad", True if we should continue processing.
    """
    if 'cki.results' in properties.get('topic', ''):
        return True

    if properties.get('attribute') != 'state':
        return False

    # We are not interested in messages that don't mark a completed task. Also,
    # official koji builds like to use numbers.
    if properties.get('new') not in ['CLOSED', 1]:
        return False

    # We are only interested in completed builds. koji official messgaes are
    # special again.
    if properties.get('method') != 'build' and 'build_id' not in properties:
        return False

    return True


def process_copr(gitlab_queue, trigger_token, triggers, message):
    # Exclude empty automated nightly builds
    if not message.get('pkg') or not message.get('version'):
        return

    nvr = '{}-{}'.format(message['pkg'], message['version'])
    if message.get('status') != 1:
        logging.debug('COPR build for %s not successful', nvr)
        return

    logging.info('COPR build for %s found', nvr)
    copr = '{}/{}'.format(message['owner'], message['copr'])

    trigger_to_use = None
    for trigger in triggers:
        if 'result_pipe' in trigger:
            continue  # This is not the trigger we are looking for

        if re.search(r'^{}-\d+[.-](\S+[.-])+{}'.format(trigger['package_name'],
                                                       trigger['rpm_release']),
                     nvr):
            if copr in trigger.get('.coprs', []):
                trigger_to_use = trigger.copy()
                break
    if not trigger_to_use:
        logging.debug('COPR: Pipeline for %s not configured', nvr)
        return

    architecture = message['chroot'].split('-')[-1]

    trigger_to_use['owner'] = message['user']
    trigger_to_use['nvr'] = nvr
    trigger_to_use['copr_build'] = str(message['build'])
    trigger_to_use['title'] = f'COPR: {nvr}: {architecture}'
    trigger_to_use['arch_override'] = architecture
    trigger_to_use['repo_name'] = copr
    trigger_to_use['cki_pipeline_type'] = 'copr'

    trigger_to_use = pt_utils.clean_config(trigger_to_use)
    put_gitlab_queue(gitlab_queue, trigger_token, trigger_to_use)


def process_result_message(gitlab_queue, trigger_token, triggers, message):
    trigger_to_use = None
    for trigger in triggers:
        if str(trigger.get('result_pipe')) == 'True':
            trigger_to_use = trigger.copy()

    if not trigger_to_use:
        logging.info('No trigger found to handle results message!')
        logging.debug('%s', message)
        return

    team = message.get('team_name', 'UNKNOWN')
    team_email = message.get('team_email', '')
    original_pipeline_id = message['cki_pipeline_id']
    if message.get('summarized_result', '').lower() in ['pass', 'success']:
        status = 'success'
    else:
        status = 'fail'

    trigger_to_use.update({
        'status': status,
        'data': base64.b64encode(
            lzma.compress(json.dumps(message['results']).encode())
        ).decode(),
        'cki_pipeline_type': 'result',
        'title': f'Results from {team} for #{original_pipeline_id}',
        'original_pipeline_id': original_pipeline_id,
        'team_name': team,
        'team_email': team_email
    })

    # Retrieve the token if we are using a different repository for this, which
    # is most likely the case
    token_name = trigger_to_use.pop('token_name', None)
    if token_name:
        real_token = utils.get_env_var_or_raise(token_name)
    else:
        real_token = trigger_token

    trigger_to_use = pt_utils.clean_config(trigger_to_use)
    put_gitlab_queue(gitlab_queue, real_token, trigger_to_use)


def process_message(gitlab_queue, trigger_token, brew_config, server_section,
                    message):
    if 'task_id' in message:
        task_id = message['task_id']  # Koji official builds
    else:
        task_id = message['id']  # Koji scratch builds and Brew
    if not task_id:   # Some koji builds don't have related tasks
        return
    logging.debug('%s: A build completed!', task_id)

    if not message.get('request'):
        logging.info('%s: Task doesn\'t have a build request, ignoring',
                     task_id)
        return

    trigger_to_use = get_brew_trigger_variables(task_id, brew_config,
                                                server_section)

    if trigger_to_use:
        put_gitlab_queue(gitlab_queue, trigger_token, trigger_to_use)


class AMQPReceiver(proton.handlers.MessagingHandler):
    def __init__(self, cert_path, urls, topics, server_section, queue):
        super(AMQPReceiver, self).__init__()

        self.cert_path = cert_path
        self.urls = urls
        self.topics = topics
        self.server_section = server_section
        self.queue = queue

    def on_start(self, event):
        ssl = proton.SSLDomain(proton.SSLDomain.MODE_CLIENT)
        ssl.set_credentials(self.cert_path, self.cert_path, None)
        ssl.set_trusted_ca_db('/etc/ssl/certs/ca-bundle.crt')
        ssl.set_peer_authentication(proton.SSLDomain.VERIFY_PEER)
        conn = event.container.connect(urls=self.urls, ssl_domain=ssl)

        for topic in self.topics:
            event.container.create_receiver(conn, source=topic)

    def on_message(self, event):
        message = json.loads(event.message.body)
        properties = event.message.properties
        if 'info' in message:
            message = message['info']

        if sanity_check(properties):
            self.queue.put((message, self.server_section))

    def on_link_opened(self, event):
        logging.info('Link opened to %s at address %s',
                     event.connection.hostname,
                     event.link.source.address)

    def on_link_error(self, event):
        logging.error('Link error: %s: %s',
                      event.link.remote_condition.name,
                      event.link.remote_condition.description)
        logging.info('Closing connection to %s', event.connection.hostname)
        event.connection.close()
        raise Exception('Link error occured!')

    def on_connection_error(self, event):
        proton.handlers.EndpointStateHandler.print_error(event.connection,
                                                         'connection')
        event.connection.close()
        raise Exception('Connection error occured!')


class ZMQReceiver:
    def __init__(self, urls, topics, server_section, queue):
        self.server_section = server_section
        self.queue = queue

        context = zmq.Context()
        self.sock = context.socket(zmq.SUB)
        for url in urls:
            self.sock.connect(url)
        for topic in topics:
            self.sock.setsockopt_string(zmq.SUBSCRIBE, topic)

        self.poller = zmq.Poller()
        self.poller.register(self.sock, zmq.POLLIN)

    def receive_messages(self):
        while True:
            self.poller.poll()
            topic, message_data = self.sock.recv_multipart()
            message = json.loads(message_data).get('msg')

            if 'copr' in topic.decode():
                self.queue.put((message, None))
            elif sanity_check(message):
                if 'info' in message:  # Only scratch builds
                    message = message['info']
                self.queue.put((message, self.server_section))


def get_triggers_from_config(brew_config):
    triggers = []
    for key, value in \
            pt_utils.process_config_tree(brew_config).items():
        trigger = copy.deepcopy(value)
        trigger['name'] = key
        triggers.append(trigger)
    return triggers


def poll_triggers(gitlab_instance, brew_config, trigger_token):
    # Create the actual triggers and start the receiver
    triggers = get_triggers_from_config(brew_config)

    queue = Queue()
    gitlab_queue = Queue()
    executor = ThreadPoolExecutor(max_workers=10)
    futures = []

    if brew_config.get('.zmq'):
        zmq_receiver = ZMQReceiver(
            brew_config['.zmq']['.receiver_urls'],
            brew_config['.zmq']['.message_topics'],
            '.zmq',
            queue
        )
        futures.append(executor.submit(zmq_receiver.receive_messages))

    if brew_config.get('.amqp'):
        logging.getLogger('proton').setLevel(logging.WARNING)
        logging.getLogger('proton.reactor').setLevel(logging.WARNING)
        logging.getLogger('proton.handlers').setLevel(logging.WARNING)
        amqp_receiver = Container(AMQPReceiver(
            brew_config['.amqp']['.cert_path'],
            brew_config['.amqp']['.receiver_urls'],
            brew_config['.amqp']['.message_topics'],
            '.amqp',
            queue
        ))
        futures.append(executor.submit(amqp_receiver.run))

    for _ in range(4):
        futures.append(executor.submit(process_queue,
                                       queue,
                                       gitlab_queue,
                                       trigger_token,
                                       triggers,
                                       brew_config))

    futures.append(executor.submit(queue_watcher, queue))
    futures.append(executor.submit(gitlab_trigger,
                                   gitlab_queue,
                                   gitlab_instance))

    for finished in as_completed(futures):
        logging.error('Thread exited unexpectedly!')
        try:
            result = finished.result()
        except Exception as exc:
            logging.error('Got exception: %s', exc)
        else:
            logging.error('Got result: %s', result)
        os._exit(1)  # How to actually kill all threads... sigh
